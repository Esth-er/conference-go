import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Use the Pexels API
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    return data["photos"][0]["src"]["original"]


def get_coordinates(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US$$limit=5&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]
    return {"lat": lat, "lon": lon}


def get_weather_data(lat, lon):
    # Use the Open Weather API
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    return {
        "description": response.json()["weather"][0]["description"],
        "temp": response.json()["main"]["temp"],
    }
